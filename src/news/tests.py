#coding: utf-8

from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import News,Media,Comments

########################################################################################
######### Tests Index ################################################################


class IndexViewTest(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('homepage'))
        self.news = News.objects.create(
            title = u'Django é D+',
            content = u'E tenho dito!'
        )
    def test_get_index_page(self):
        "Verifica rota index.html"
        self.assertTemplateUsed(self.response, 'index.html')

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_title_in_context(self):
        self.assertEqual(self.news.title,u'Django é D+')

    def test_content_in_context(self):
        self.assertEqual(self.news.content,u'E tenho dito!')

    def test_news_in_context(self):
        self.assertIn('news',self.response.context)


########################################################################################
######### Tests News ################################################################


class TestModelNews(TestCase):
    def test_model_news(self):
        s = News.objects.create(
            title = 'Django é D+',
            content = 'E tenho dito!'
        )
        self.assertEquals(s.id,1)

class NewsDetailView(TestCase):
    def setUp(self):
        News.objects.create(
            title='Django legal',
            slug='django-legal',
            content='lero-lero'
        )
        self.response = self.client.get(reverse('news:news_detail',
            kwargs={'slug': 'django-legal'}))

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'news/news_detail.html')

    def test_news_in_context(self):
        news = self.response.context['news']
        self.assertIsInstance(news,News)


    def test_form_in_context(self):
        self.assertIn('form',self.response.context)




########################################################################################
######### Test Media ####################################################################

class MediaModelTest(TestCase):
    def setUp(self):
        news = News.objects.create(
            title='Django',
            slug='django-legal',
            content='lero-lero'
        )
        self.media = Media.objects.create(
            news = news,
            type='YT',
            media_id='QjA5faZF1A8',
            title='Video'
        )

    def test_create(self):
        self.assertEqual(1,self.media.pk)

    def test_unicode(self):
        self.assertEqual("Django - Video", unicode(self.media))

########################################################################################
######### Test Comment ####################################################################

class CommentModelTest(TestCase):
    comment = []
    def setUp(self):
        news = News.objects.create(
            title='Django',
            slug='django-legal',
            content='lero-lero'
        )
        self.comment.append(Comments.objects.create(
            news = news,
            name = 'Charles',
            email = 'charles.sartori@gmail.com',
            comment = 'lero-lero'
        ))
        self.comment.append(Comments.objects.create(
            news = news,
            name = 'Charles2',
            email = 'charles.sartori@gmail.com2',
            comment = 'lero-lero2'
        ))
    def test_create(self):
        self.assertEqual(1,self.comment[0].pk)

    def test_ordem(self):
        ordem = Comments.objects.all()
        self.assertEqual(ordem[0].name,'Charles2')

    def test_unicode(self):
        self.assertEqual('Charles', unicode(self.comment[0].name))



