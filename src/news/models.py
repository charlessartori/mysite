#coding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _


########################################################################################
######### Model News ################################################################


class NewsManager(models.Manager):
    def get_query_set(self):
        qs = super(NewsManager, self).get_query_set()
        qs = qs.order_by("created_at").reverse()
        return qs

class News(models.Model):
    title = models.CharField(_(u'Título'), max_length=100)
    slug = models.SlugField(unique=True)
    content = models.TextField(_(u'Conteúdo'), max_length=600)
    created_at = models.DateTimeField(_(u'Criado em'), auto_now_add=True)

    objects = NewsManager()

    @property
    def slides(self):
        return self.media_set.filter(type='SL')

    @property
    def videos(self):
        return self.media_set.filter(type='YT')

    @property
    def comments(self):
        return self.comments_set.order_by('created_at').reverse()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Notícia"
        verbose_name_plural = u"Notícias"

########################################################################################
######### Model Media ################################################################

class Media(models.Model):
    MEDIAS = (
        ('SL','SlideShare'),
        ('YT', 'Youtube'),
        )

    news = models.ForeignKey('News')
    type = models.CharField(max_length=2, choices=MEDIAS)
    title = models.CharField(_(u'Título'), max_length=255)
    media_id = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s - %s' % (self.news.title, self.title)

##########################################################################################
######### Model Comentário ################################################################


class CommentsManager(models.Manager):
    def get_query_set(self):
        qs = super(CommentsManager, self).get_query_set()
        qs = qs.order_by("created_at").reverse()
        return qs

class Comments(models.Model):
    news = models.ForeignKey('News')
    name = models.CharField(_(u'Nome'),max_length=100)
    email = models.EmailField(_(u'E-mail'),blank=True)
    comment = models.TextField(_(u'Comentário'))
    created_at = models.DateTimeField(_(u'Criado em'), auto_now_add=True)

    objects = CommentsManager()


    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Comentário"
        verbose_name_plural = u"Comentários"