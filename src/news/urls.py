#coding: utf-8

__author__ = 'charles'

from django.conf.urls import patterns,include,url

urlpatterns = patterns('src.news.views',
    url(r'^news/(?P<slug>[\w-]+)/$', 'news_detail',
        name='news_detail'),
    url(r'^sucesso/', 'comment_sent', name='comment_sent'),
)