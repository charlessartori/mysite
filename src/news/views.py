#coding: utf-8

from django.shortcuts import get_object_or_404
from django.views.generic.simple import direct_to_template
from .models import News,Comments
from .forms import CommentForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse



def homepage(request):
    news = News.objects.all()
    return direct_to_template(request, 'index.html',
            {'news' : news})

def news_detail(request, slug):
    if request.method == 'POST':
        return create(request,slug)
    else:
        news = get_object_or_404(News,slug=slug)
        form = CommentForm(initial={'news':news})
        return direct_to_template(request, 'news/news_detail.html',
                                {'news' : news,
                                 'form' : form})

def create(request,slug):
    news = get_object_or_404(News,slug=slug)
    form = CommentForm(request.POST ,initial={'news':news})

    if not form.is_valid():
        return direct_to_template(request, 'news/news_detail.html',
                {'news' : news,
                 'form' : form})

    comment = form.save()
    return HttpResponseRedirect(reverse('news:comment_sent'))

def comment_sent(request):
    return direct_to_template(request,'news/comment_sent.html',{})