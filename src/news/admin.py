#coding: utf-8

__author__ = 'charles'

from django.contrib import admin
from .models import News, Media, Comments
from django.utils.datetime_safe import datetime

class MediaInline(admin.TabularInline):
    model = Media
    extra = 1

class NewsAdmin(admin.ModelAdmin):
    list_display = ('title','slug','content','created_at','subscribed_today')
    date_hierarchy = 'created_at'
    search_fields = ('title','content')
    list_filter = ['created_at']
    inlines = [MediaInline,]
    prepopulated_fields = {'slug': ('title',)}

    def subscribed_today(self, obj):
        return obj.created_at.date() == datetime.today().date()
    subscribed_today.short_description = u'Adicionado hoje?'
    subscribed_today.boolean = True

    class Media:
        js = ('/static/js/tiny_mce/tiny_mce.js', '/static/js/tiny_mce/textareas.js',)

class CommentAdmin(admin.ModelAdmin):
    list_display = ('news','name','email','comment','created_at','subscribed_today')
    date_hierarchy = 'created_at'
    search_fields = ('name','comment','email')
    list_filter = ['created_at']


    def subscribed_today(self, obj):
        return obj.created_at.date() == datetime.today().date()
    subscribed_today.short_description = u'Adicionado hoje?'
    subscribed_today.boolean = True


admin.site.register(News, NewsAdmin)
admin.site.register(Comments, CommentAdmin)
