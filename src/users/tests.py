#coding: utf-8

from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import User, Contact, Skills, Course, Message
from .forms import MessageForm


########################################################################################
######### Tests User ################################################################


class UserModelTest(TestCase):
    def setUp(self):
        self.user = User(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )
        self.user.save()

    def test_create(self):
        self. assertEqual(1,self.user.pk)

    def test_unicode(self):
        self.assertEqual(u'Charles Sartori', unicode(self.user))

########################################################################################
######### Tests Contact ################################################################


class ContactModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo Ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

    def test_create_email(self):
        contact = Contact.objects.create(
            user=self.user,
            kind='E',
            value ='charles.sartori'
        )
        self.assertEqual(1,contact.pk)

    def test_create_phone(self):
        contact = Contact.objects.create(
            user=self.user,
            kind='P',
            value ='55-91416848'
        )
        self.assertEqual(1,contact.pk)

    def test_create_facebook(self):
        contact = Contact.objects.create(
            user=self.user,
            kind='F',
            value ='charles.pexe'
        )
        self.assertEqual(1,contact.pk)

class ContactView(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('users:contact_detail'))
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo Ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'users/contact_detail.html')

    def test_user_in_context(self):
        self.assertEqual(self.user.name, u'Charles Sartori')

    def test_has_form(self):
        self.assertIsInstance(self.response.context['form'],MessageForm)

    def test_form_has_fields(self):
        form = self.response.context['form']
        self.assertItemsEqual(['name','email','message'],form.fields)


class ContactViewPostTest(TestCase):
    def setUp(self):
        data = dict(
            name='Charles Sartori',
            email='charles.sartori@gmail.com',
            message='Ola Charles'
        )
        self.response = self.client.post(reverse('users:contact_detail'),data)

    def test_redirects(self):
        self.assertRedirects(self.response, reverse('users:message_sent'))

    def test_save(self):
        self.assertTrue(Message.objects.exists())



########################################################################################
######### Tests Course ################################################################

class CourseModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

    def test_course_create(self):
        course = Course.objects.create(
            user = self.user,
            title = 'Curso 1',
            local = 'Santo Angelo',
            date = '1989-06-18',
            duration = '1 ano'
        )
        self.assertEqual(1,course.pk)

class CourseView(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('users:courses_detail'))
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )
    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'users/course_detail.html')

    def test_user_in_context(self):
        self.assertEqual(self.user.name,u'Charles Sartori')

########################################################################################
######### Tests Skills ################################################################


class SkillsModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

    def test_level_basico(self):
        skills = Skills.objects.create(
            user =self.user,
            title = 'Ciência da Computação',
            level = 'B',
            description = 'Universidade URI'
        )
        self.assertEqual(1,skills.pk)

    def test_level_intermediario(self):
        skills = Skills.objects.create(
            user =self.user,
            title = 'Ciência da Computação',
            level = 'I',
            description = 'Universidade URI'
        )
        self.assertEqual(1,skills.pk)

    def test_level_avancado(self):
        skills = Skills.objects.create(
            user =self.user,
            title = 'Ciência da Computação',
            level = 'A',
            description = 'Universidade URI'
        )
        self.assertEqual(1,skills.pk)

class SkillsView(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('users:skills_detail'))
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'users/skills_detail.html')

    def test_user_in_context(self):
        self.assertEqual(self.user.name, u'Charles Sartori')

########################################################################################
######### Tests About ################################################################

class AboutView(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('users:about_detail'))
        self.user = User.objects.create(
            name = 'Charles Sartori',
            slug = 'charles-sartori',
            url = 'http://google.com',
            city = 'Santo ângelo',
            state = 'RS',
            birthday = '1989-06-18'
        )

def test_get(self):
    self.assertEqual(200,self.response.status_code)

def test_template(self):
    self.assertTemplateUsed(self.response,'users/about_detail.html')

def test_user_in_context(self):
    self.assertEqual(self.user.name, u'Charles Sartori')

########################################################################################
######### Tests Message ################################################################

class ModelMessage(TestCase):
    def test_create(self):
        self.message = Message.objects.create(
            name = 'Charles Sartori',
            email = 'charles.sartori@gmail.com',
            message = 'Ola Charles'
        )
        self.assertEqual(1,self.message.pk)

class MessageViewSent(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('users:message_sent'))

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'users/message_sent.html')


























