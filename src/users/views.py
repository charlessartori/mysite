from datetime import date
from django.views.generic.simple import direct_to_template
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import User
from .forms import MessageForm


def skills_detail(request):
    users = User.objects.all()
    return direct_to_template(request, 'users/skills_detail.html',
            {'users': users})


def courses_detail(request):
    users = User.objects.all()
    return direct_to_template(request, 'users/course_detail.html',
            {'users': users})

def about_detail(request):
    users = User.objects.all()
    return direct_to_template(request, 'users/about_detail.html',
            {'users': users})

def message_sent(request):
    return direct_to_template(request, 'users/message_sent.html',
            {})

def contact_detail(request):
    if request.method == 'POST':
        return create(request)
    else:
        users = User.objects.all()
        return direct_to_template(request, 'users/contact_detail.html',
                {'users': users,
                 'form' : MessageForm()})

def create(request):
    form = MessageForm(request.POST)

    if not form.is_valid():
        users = User.objects.all()
        return direct_to_template(request, 'users/contact_detail.html',
                {'users': users,
                 'form' : form})

    message = form.save()
    return HttpResponseRedirect(reverse('users:message_sent'))