#coding: utf-8
__author__ = 'charles'

from django import forms
from .models import Message

class MessageForm(forms.ModelForm):
    name = forms.CharField(required=True)
    class Meta:
        model = Message