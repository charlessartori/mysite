__author__ = 'charles'

from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('src.users.views',
    url(r'^contato/', 'contact_detail', name='contact_detail'),
    url(r'^skills/', 'skills_detail', name='skills_detail'),
    url(r'^formacao/', 'courses_detail', name='courses_detail'),
    url(r'^sobre/', 'about_detail', name='about_detail'),
    url(r'^sucesso/', 'message_sent', name='message_sent'),

)