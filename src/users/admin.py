#coding: utf-8
__author__ = 'charles'

from django.contrib import admin
from .models import User,Contact,Skills,Course,Message

class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1

class SkillsInline(admin.TabularInline):
    model = Skills
    extra = 1

class CourseInline(admin.TabularInline):
    model = Course
    extra = 1

class UserAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    search_fields = ('name',)
    inlines = [ContactInline,
               CourseInline,
               SkillsInline,
               ]
    prepopulated_fields = {'slug':('name',)}

class MessageAdmin(admin.ModelAdmin):
    list_display = ('name','email','message')

admin.site.register(User,UserAdmin)
admin.site.register(Message,MessageAdmin)