#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.

########################################################################################
######### Model User ################################################################



class User(models.Model):
    STATE = (
        ('RS',_('Rio Grande do Sul')),
    )
    name = models.CharField(_(u'Nome'), max_length=255, blank=False)
    slug = models.SlugField(_(u'Slug'), blank=False)
    url = models.URLField(_(u'Url'),null=True, blank=True)
    city = models.CharField(_(u'Cidade'),max_length=255, null=True, blank=True)
    state = models.CharField(_(u'Estado'),max_length=2,choices=STATE, null=True, blank=True)
    birthday = models.DateField(_(u'Data de Nascimento'), null=True, blank=True)
    description = models.TextField(_(u'Descrição'), null=True, blank=True)
    avatar = models.FileField(_(u'Avatar'),upload_to='users',null=True, blank=True)

    def __unicode__(self):
        return self.name

    @property
    def contacts(self):
        return self.contact_set.all()

    @property
    def courses(self):
        return self.course_set.all()

    @property
    def skills(self):
        return self.skills_set.all()

    class Meta:
        verbose_name = u"Usuário"
        verbose_name_plural = u"Usuários"


########################################################################################
######### Model Skills ################################################################


class Skills(models.Model):
    LEVEL = (
        ('B',_(u'Básico')),
        ('I',_(u'Intermediário')),
        ('A',_(u'Avançado')),
        )
    user = models.ForeignKey('User', verbose_name=_(u"Usuário"))
    title = models.CharField(_(u'Título'), max_length=255, null=True, blank=True)
    level = models.CharField(_(u'Nível'),max_length=1,choices=LEVEL, null=True, blank=True)
    description = models.TextField(_(u'Descrição'), null=True, blank=True)


    class Meta:
        verbose_name = u"Skill"
        verbose_name_plural = u"Skills"

########################################################################################
######### Model Course ################################################################

class CourseManager(models.Manager):
    def get_query_set(self):
        qs = super(CourseManager,self).get_query_set()
        qs = qs.order_by("date").reverse()
        return qs

class Course(models.Model):
    user = models.ForeignKey('User', verbose_name=_(u"Usuário"))
    title = models.CharField(_(u'Título'), max_length=255, null=True, blank=True)
    local = models.CharField(_(u'Local'), max_length=255, null=True, blank=True)
    date = models.DateField(_(u'Data de início'), null=True, blank=True)
    duration = models.CharField(_(u'Duração'), max_length=50, null=True, blank=True)

    objects = CourseManager()

    class Meta:
        verbose_name = u"Curso"
        verbose_name_plural = u"Cursos"

########################################################################################
######### Model Contact ################################################################

class KindContactManager(models.Manager):
    def __init__(self, kind):
        super(KindContactManager, self).__init__()
        self.kind = kind

    def get_query_set(self):
        qs = super(KindContactManager, self).get_query_set()
        qs = qs.filter(kind=self.kind)
        return qs



class Contact(models.Model):
    KINDS = (
        ('P',_(u'Telefone')),
        ('E',_(u'E-mail')),
        ('F',_(u'Facebook')),
        ('S',_(u'Skype')),
    )

    user = models.ForeignKey('User', verbose_name=_(u"Usuário"))
    kind = models.CharField(_(u'Tipo'), max_length=1,choices=KINDS)
    value = models.CharField(_(u'Valor'), max_length=255)

    objects = models.Manager()
    phone = KindContactManager('P')
    email = KindContactManager('E')
    facebook = KindContactManager('F')

    class Meta:
        verbose_name = u"Contato"
        verbose_name_plural = u"Contatos"


########################################################################################
######### Model Message ################################################################

class Message(models.Model):
    name = models.CharField(_(u'Nome'), max_length=255)
    email = models.EmailField(_(u'E-mail'),blank=True)
    message = models.TextField(_(u'Mensagem'))
    created_at = models.DateTimeField(_(u'Criado em'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["created_at"]
        verbose_name = u"Mensagem"
        verbose_name_plural = u"Mensagens"


